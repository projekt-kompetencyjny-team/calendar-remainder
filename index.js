const express = require('express');
const path = require('path');
const session = require('express-session');
const bodyParser = require('body-parser');
const url = require('url');
const moment = require('moment');

let pgp = require('pg-promise')();
let db = pgp('postgres://pmiewunz:L1HpzxmlpT1AgeQIiM8A_41KPlvoCIlG@manny.db.elephantsql.com:5432/pmiewunz');

const port = 3000;
const app = express();

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname + '/views'));

app.use(session({
    secret: '@#ffg42g244t2g2f24f2$@t@g42$G24@f@vVFsG$%^HBDFB%4BBe5B#346ybdgD5DH5',
    resave: true,
    saveUninitialized: true,
    cookie: {
        maxAge: 600000
    }
}));

app.use(bodyParser.json()) // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

app.get('/', (req, res) => {
    res.redirect('/login');
});

app.get('/calendar', (req, res) => {
    if(req.session.loggedid) {
        //console.log(req.session.loggedid);
        db.manyOrNone("SELECT event_id, title, timestamp_deadline FROM events WHERE user_id = $1", req.session.loggedid).then(result => {
            let parseData = [];

            for(let i = 0; i < result.length; i++) {

                let tempTime = moment(result[i].timestamp_deadline);

                parseData.push({
                    id: result[i].event_id,
                    title: result[i].title,
                    time: tempTime
                });
            }

            res.render(path.join('index.ejs'), {
                user: req.session.login,
                results: parseData
            });
        });
    }
    else
        res.redirect('/login');
});

app.get('/login', (req, res) => {
    res.sendFile(path.join(__dirname + '/login.html'));
});

app.post('/auth', (req, res) => {
    let username = req.body.username;
    let password = req.body.password;
    if(username && password) {
        db.oneOrNone("SELECT user_id, login FROM Users WHERE login = $1 AND acc_password = $2", [username, password]).then(data => {
            req.session.loggedid = data.user_id;
            req.session.login = data.login;
            console.log(data.login);

            res.redirect(url.format({
                pathname: "/calendar"
            }));
        }).catch(error => {
            res.redirect('/login_error');
        });
    }
});

app.get('/login_error', (req, res) => {
    res.sendFile(path.join(__dirname + '/loginerror.html'));
});

app.delete('/delchit/:id', (req, res) => {
     if(req.session.loggedid) {
         console.log(req.params.id);
         db.none("DELETE FROM events WHERE event_id = $1", req.params.id).then(() => {
             console.log("done LOL");
             res.redirect("/calendar");
         });
     }
     else
         res.redirect('/login');
});

app.post('/addnew/:title-:date', (req, res) => {
    if(req.session.loggedid) {
        console.log("no dodajemy");
        console.log(req.session.loggedid);
        console.log(req.params.title);
        console.log(req.params.date);
        db.none("INSERT INTO events (user_id, type_of_event, title, timestamp_deadline, ev_state) " +
            "VALUES("+req.session.loggedid+", 'other', '"+req.params.title+"', '"+req.params.date+"', 'waiting')").then(() => {
            console.log("udalo sie?");
            res.redirect("/calendar");
        });
    }
    else
        res.redirect('/login');
});

app.get('/logout', (req, res) => {
    if(req.session.loggedid) {
        console.log("destroy");
        req.session.destroy();
        res.redirect('/login');
    }
});

app.post('/register', (req, res) => {
    console.log(req.body.first_name);
    db.none("INSERT INTO users (first_name, surname, email, login, acc_password)" +
        " VALUES('"+req.body.first_name+"', '"+req.body.last_name+"','"+req.body.email+"', '"+req.body.nickname+"', '"+req.body.password+"')").then(()=> {
            console.log("poszlo");
        res.redirect('/login');
    });
});

app.use(express.static('views'));
app.listen(port, () => console.log('Example app listining on port '+port+'!'));
