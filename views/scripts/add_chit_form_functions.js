function minDateInput() {
    var dtToday = new Date();

    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();

    return year + '-' + month + '-' + day;
}

function getDateForm() {
    let date = dateInput.value;
    let hour = hourInput.value;
    let minutes = minutesInput.value;

    return date + " " + hour + ":" + minutes;
}
