function addNewChit(cardsContainer, id, text, date, init) {
    if(text.length <= 0)
        return;

    if(init === false) {
        $.ajax({
            url: '/addnew/'+text+'-'+date,
            type: 'POST',
            success: () => {
                location.reload();
            }
        });
    } else {

        let children = cardsContainer.children;

        let less = children[0].children.length;
        let indexLess = 0;

        for(let i = 1; i < children.length; i++) {
            if(less > children[i].children.length) {
                less = children[i].children.length;
                indexLess = i;
            }
        }

        let divCard = createNode('div', 'card mt-3');
        divCard.id = id;
        let cardBody = createNode('div','card-body');
        let underRow = createNode('div','row no-gutters');
        let col10 = createNode('div','col-10');
        let col2 = createNode('div','col-2');
        let cardTime = createNode('h5','card-title');

        let cardText = createNode('p','card-text');
        cardText.innerHTML = text;

        cardBody.appendChild(cardTime);
        cardBody.appendChild(underRow);
        underRow.appendChild(col10);
        underRow.appendChild(col2);
        col10.appendChild(cardTime);
        cardBody.appendChild(cardText);
        divCard.appendChild(cardBody);
        children[indexLess].appendChild(divCard);

        appendImage(col2, id);

        formTextarea.value = '';

        var countDownDate = new Date(date).getTime();

        // Get today's date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Display the result in the element with id="demo"
        cardTime.innerHTML = days + "d " + hours + "h "
            + minutes + "m " + seconds + "s ";

        if (distance < 0) {
            clearInterval(x);
            cardTime.innerHTML = "EXPIRED";
        }

    // Update the count down every 1 second
        var x = setInterval(function() {
            now = new Date().getTime();

            // Find the distance between now and the count down date
            distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            days = Math.floor(distance / (1000 * 60 * 60 * 24));
            hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Display the result in the element with id="demo"
            cardTime.innerHTML = days + "d " + hours + "h "
                + minutes + "m " + seconds + "s ";

            // If the count down is finished, write some text
            if (distance < 0) {
                clearInterval(x);
                cardTime.innerHTML = "EXPIRED";
            }
        }, 1000);
    }
}

function setVisibleChitBox(addChitBox, background) {
    addChitBox.style.visibility = 'visible';
    background.style.visibility = 'visible';
}

function setCollapseChitBox(addChitBox, background) {
    addChitBox.style.visibility = 'collapse';
    background.style.visibility = 'collapse';
}

function deleteChit(id) {
    $.ajax({
        url: '/delchit/'+id,
        type: 'DELETE',
        success: () => {

        }
    });
    location.reload();
}

function logout() {
    console.log("essa");
    $.ajax({
        url: '/logout',
        type: 'GET'
    });
    window.location.href = "/login";
}

function createNode(nodeName, className) {
    let node = document.createElement(nodeName);
    node.className = className;
    return node;
}

function appendImage(container, idDiv) {
    container.innerHTML = "<a href='#' onclick='deleteChit(" + idDiv + ")'><img src='img/bin.png' alt='kosz' class='ml-3' style='width: 16px; height: 22px;' /></a>";
}

function test() {
    console.log(document.getElementById('dateInput').value);
}

function checkHour(element) {
    let hour = parseInt(element.value);
    if(!Number.isInteger(hour)) {
        element.style.border = "1px red solid";
        return;
    }

    if(hour > 23 || hour < 0) {
        element.style.border = "1px red solid";
        return;
    }

    element.style.border = "1px green solid";
}

function checkMinute(element) {

    let minute = parseInt(element.value);
    console.log(element.value);
    if(Number.isNaN(minute)) {
        console.log("tu");
        element.style.border = "1px red solid";
        return;
    }

    if(minute > 59 || minute < 0) {
        console.log("tu1");
        element.style.border = "1px red solid";
        return;
    }

    $('#buttonFormAdd').attr("disabled", false);
    element.style.border = "1px green solid";
}

